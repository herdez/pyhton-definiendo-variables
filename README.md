Python es un lenguaje de programación de alto nivel, interactivo e interpretado, creado por Guido Van Rossum en 1991. Python es un lenguaje en el que todos sus elementos son objetos, incluyendo los tipos de datos básicos.

Algunas aplicaciones de este lenguaje son:

- Aplicaciones de escritorio.
- Aplicaciones web.
- Análisis de datos.
- Administración de servidores.
- Seguridad y análisis de penetración.
- Cómputo en la nube.
- Cómputo científico.
- Análisis de lenguaje natural.
- Visión artificial.
- Animación, videojuegos e imágenes generadas por computadora.
- Aplicaciones móviles.

## Python y Tipos de Datos

En Python todos sus elementos son objetos y los datos una vez identificados, se convierten en objetos instanciados del tipo al que pertenecen. Estos son los tipos de datos más usados en Python:

- Numéricos: entero (int), flotante (float), complejo (complex).
- Booleans (bool)
- Strings (str)
- None
- Lists
- Tuples
- Dict

#### Tipo entero (int)

Los números enteros son aquellos que no tienen decimales, tanto positivos como negativos (además del cero). En Python se pueden representar mediante el tipo int (de integer, entero) o el tipo long (largo). La única diferencia es que el tipo long permite almacenar números más grandes. 

```python
# type int
entero = 27
```

Python 2 identifica al tipo llamado entero largo (long), al cual se le añadía la letra "L" al final, pero ya no son reconocidos por Python 3.

```python
# type long
entero = 27L
```

El literal que se asigna a la variable también se puede expresar como un octal, anteponiendo un cero:

```python
# 027 octal = 23 en base 10
entero = 027
```

También puede ser hexadecimal, anteponiendo un 0x:

```python
# 0x17 hexadecimal = 23 en base 10
entero = 0x17
```

#### Tipo Flotante (float)

Algunos ejemplos de flotantes son:

```
73.244
0.0005
-370.467
0.0
```

La precisión de los números dependen en gran medida de la capacidad del equipo de cómputo. En ocasiones una operación con números de tipo float no dará el resultado exacto, sino una aproximación.

Para aplicaciones normales podeis utilizar el tipo float, como ha venido haciéndose desde hace años, aunque teniendo en cuenta que los números tipo flotante no son precisos (ni en este ni en otros lenguajes de programación).


#### Tipo Complejo (complex)

Los números complejos son aquellos que tienen parte imaginaria. Los números complejos en Python se representan de la siguiente forma:

```python
complejo1 = 4.32 + 55j
complejo2 = 0.125j
complejo3 = (5 + 0j)
complejo4 = 1j
```

Cuando el componente en los números reales sea distinto de 0, los objetos de tipo complex se expresarán como un par de números de tipo float separados por el operador de adición `"+"`.

#### Tipo Boolean

El tipo de dato booleano es aquel que solamente puede tomar dos posibles valores. Si la expresión lógica es cierta, el resultado es True (con mayúscula al principio). Si la expresión lógica NO es cierta, el resultado es False (con mayúscula al principio).

False equivale numéricamente a 0. Cualquier otro número equivale a True y su valor por defecto es 1.

```
>>> x = False
>>> x
False
>>> y = True
>>> y
True
>>> 200 == 200
True
>>> 5 > 10
False
```

#### String (str)

Los strings o cadenas de texto son secuencias de caracteres. La sintaxis para definirlos es utilizando comillas simples o comillas dobles.

Cualquier caracter puede ser parte de un string, desde números '67', hasta caracteres como '!' '@' '^` etc.

```
"Hello World!"
'Ruby y Python'
"hello/$!^"
"1024"
```

#### Tipo None

Este tipo representa un valor `vacío`.

#### Tipo List

La lista en Python son variables que almacenan arrays, internamente cada posición puede ser un tipo de dato distinto. Se encuentran encerrados entre corchetes ([ ]).

```python
["Carlos", 11, true, "names"]
[6, 3, 10, 100]
l = [2, "tres", True, ["uno", 10]]
[]
[{"nombre": "carlos"}, [24, 56], "colonia", "ciudad"]
```

No son equivalentes a las matrices en otros lenguajes de programación.

#### Tipo Tuples

Una tupla es una lista inmutable. Una tupla no puede modificarse de ningún modo después de su creación. Los objetos encuentran separados por comas y encerrados entre paréntesis `"( )"`.


```
>>> ('fruta', 'animal', True)
('fruta', 'animal', True)
>>> (['fruta', 10, 'fresa'], ['computadora', 700, 'mac'])
(['fruta', 10, 'fresa'], ['computadora', 700, 'mac'])
>>> ()
()
>>> tupla = 12345, 54321, 'hello!'
>>> tupla
(12345, 54321, 'hello!')
>>> otra_tupla = tupla, (10, 34, 21)
>>> otra_tupla
((12345, 54321, 'hello!'), (10, 34, 21))
```

#### Tipo Dict

El diccionario define una relación uno a uno entre claves y valores ('clave-valor' o 'key-value') como "nombre": "sandra".

La clave (key) puede ser un objeto inmutable tal como es el caso de:

- Objetos de tipo str.
- Objetos de tipo int.
- Objetos de tipo float.
- Objetos de tipo complex.
- Objetos de tipo bool.
- Objetos de tipo tuple.


```python
months = {"3": "March", "4": "April"}
estudiante = { "first_name": "Marcos", "last_name": "Herran", "age": 35 }
{23:('Fruta', True, 23), "ropa":None}
{2.45: 'hello', (23, 56): 'numeros', 34j: True}
{}
``` 

### Funciones relativas a tipos de datos

#### type()
type regresa el tipo de dato de una variable.

```
>>> type("Hello")
<class 'str'>
```

#### str()

Convierte el objeto a una cadena de caracteres.

```
>>> str(False)
'False'
>>> str(23 + 10.7j)
'(23+10.7j)'
```

#### int()

Convierte un objeto a un tipo de dato int. Considerando los siguiente:

- Puede convertir objetos de tipo str que representen correctamente a un número entero.
- Trunca los objetos de tipo float a la parte entera.
- True es convertido en 1 y False en 0.
- No es compatible con objetos tipo complex.

```
>>> int("-30")
-30
>>> int(23.35)
23
>>> int(-90.21)
-90
>>> int(54.7j)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can't convert complex to int
>>> int('Marco')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'Marco'
>>> int(False)
0
```

#### float()

Convierte un objeto a uno de tipo float. Considerando lo siguiente:

- Puede convertir objetos de tipo str que representen correctamente a un número real.
- Es compatible con los objetos tipo int.
- True es convertido en 1.0 y False en 0.0.
- No es compatible con objetos tipo complex.

```
>>> float(-76)
-76.0
>>> float('Fruta')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: could not convert string to float: 'Fruta'
>>> float(23.78 + 45j)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can't convert complex to float
>>> float(True)
1.0
>>> float("-23.89")
-23.89
```

#### complex()

Convierte un objeto a uno de tipo complex. Considerando lo siguiente:

- Convierte objetos de tipo str que representen correctamente a un número real.
- Convierte un objeto de tipo complex a un par de números ya sean int o float.
- Si solamente se da un número int o float, será identificado como el componente real y el componente complejo será 0j.

```
>>> complex(10.7, 5)
(10.7+5j)
>>> complex(20)
(20+0j)
>>> complex("45+9j")
(45+9j)
```

#### bool()

Convierte un objeto a un tipo de dato boolean. Considerando lo siguiente:

- El 0 es igual a False.
- Cualquier otra cosa distinto de 0 es True.

```
>>> bool("Coding")
True
>>> bool("1")
True
>>> bool(-90)
True
>>> bool(10)
True
>>> bool(0.2)
True
>>> bool(0.0)
False
```

## Variables y Constantes

Las variables y constantes en Ruby son espacios de memoria reservados que guardan los valores. Las variables como su nombre lo indica pueden variar a lo largo de la ejecución del programa.

#### Definiendo una variable en Python

Para asignar un nombre a un objeto, se utiliza el el operador de asignación `"="` con la siguiente sintaxis:

```python
saludo = 'Hello'
datos = [["nombre", "maryel", True], ["apellido", "montoya", True]] 
valor = 45.56
```

Es posible asignar a varias variables un número igual de objetos usando un sólo operador de asignación mediante la siguiente sintaxis:

```python
entero, flotante, booleano = 50, 34.50 , False
```

El lenguaje Python reconoce cualquier palabra con las siguientes características como variables:

- Python 3 acepta el uso de unicode, por lo que es posible utilizar cualquier caracter alfabético, incluso aquellos distintos al alfabeto occidental, para la elaboración de nombres.
- Los nombres pueden empezar con un guión bajo _ o un caracter alfabético.
- Después del primer caracter, se pueden utilizar caracteres alfabéticos, números y/o guiones bajos.
- No se permiten caracteres distintos a los alfabéticos o que pudieran confundirse con operadores como `"|", "~", "#", "-", etc`.
- Se pueden utilizar mayúsculas. Python es sensible a mayúsculas.

```python
_numero = 23
número_flotante = 34.56
Numero = 76
```

#### Eliminación de variables

Es posible eliminar una variable con la declaración `del`:

- Desliga a la variable de un objeto en el espacio de nombres.
- Una vez que el nombre de la variable es desligado, ya no es posible invocarlo por ese nombre.
- Cuando el objeto no está ligado a otras variables en el espacio de nombres, se podría destruir al objeto.

```
>>> a = 23
>>> a
23
>>> b = 23
>>> b
23
>>> del b
>>> b
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'b' is not defined
```

#### Definiendo una constante en Python

Para definir una constante es importante utilizar nombres descriptivos y en mayúsculas separando palabras por guiones bajos.

```python
MY_CONSTANT = 234
``` 

El valor de una constante debe permanecer sin cambios durante la ejecución de un programa, es decir no es conveniente modificarla.

# Ejercicio - Vamos Definiendo variables en Python

```python
#Define tres variable tipo entero.

#Define tres variables tipo flotante.

#Define tres variables tipo boolean.

#Define una variable y asígnale una lista (list) que tenga valores tipo string y dict.

#Define una variable y asígnale un dict con tres claves (keys) de tipo string, tupla y flotante, dos valores tipo list y un valor tipo dict con 3 elementos del tipo de dato que decidas.

```